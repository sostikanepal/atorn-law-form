<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Social;
use App\Models\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ThemeController extends Controller
{
    public function theme(){
        $theme = Theme::first();
        return view('admin.theme.theme', compact('theme'));

    }

    //update theme settings
    public function themeUpdate(Request $request, $id){
        $data = $request->all();
        $rules = [
            'website_name' => 'required|max:40',
            'website_tagline' => 'required'
        ];
        $customMessages = [
            'website_name.required' => 'Website Name is required',
            'website_tagline.required' => 'Website Tagline is required',
            'website_name.max' => 'You are not allowed to enter more than 40 characters',
        ];
        $this->validate($request, $rules,$customMessages);
        $theme = Theme::findOrFail($id);
        $theme->website_name = $data['website_name'];
        $theme->website_tagline = $data['website_tagline'];
        $theme->email = $data['email'];
        $theme->phone = $data['phone'];
        $theme->address = $data['address'];
        $theme->footer_info = $data['footer_info'];

        $random = Str::random(10);  //for generating random name of image of length not more than 10
        if($request->hasFile('logo')){
            $image_tmp = $request->file('logo');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension(); //for making sure that extension like jpg,jpeg,png,etc have their original extension
                $filename = $random . '.' .$extension;
                $image_path = 'public/uploads/'.$filename;
                Image::make($image_tmp)->save($image_path); //for saving image
                $theme->logo = $filename; //for saving image
            }
        }

        $random = Str::random(10);  //for generating random name of image of length not more than 10
        if($request->hasFile('favicon')){
            $image_tmp = $request->file('favicon');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension(); //for making sure that extension like jpg,jpeg,png,etc have their original extension
                $filename = $random . '.' .$extension;
                $image_path = 'public/uploads/'.$filename;
                Image::make($image_tmp)->save($image_path); //for saving image
                $theme->favicon = $filename; //for saving image
            }
        }
        $theme->save();
        Session::flash('success_message', 'Theme Settings has been updated successfully');
        return redirect()-> back();

    }
    public function social(){
        $social = Social::first();
        return view ('admin.theme.social', compact('social'));
    }

    public function socialUpdate(Request $request, $id){
        $data = $request->all();
        $social = Social::findOrFail($id);
        $social->facebook = $data['facebook'];
        $social->twitter = $data['twitter'];
        $social->instagram = $data['instagram'];
        $social->google_plus = $data['google_plus'];
        $social->linkedin = $data['linkedin'];
        $social->save();
        Session::flash('success_message', 'Social Settings has been updated successfully');
        return redirect()-> back();

    }

}







